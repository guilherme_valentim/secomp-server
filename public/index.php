<?php

require_once __DIR__ . '/../class-loader.php';

$logWriter = new \Slim\LogWriter(fopen('../logs/SLIM_errors.log', 'a'));
$app = new Slim\Slim(array('log.writer' => $logWriter));
$app->config('debug', true); 

$app->add(new \CorsSlim\CorsSlim());

$app->map('/:x+', function($x) {
    http_response_code(200);
})->via('OPTIONS');

//Get default
$app->get('/', function() use($app){
    $app->response->headers->set('Content-Type', 'text/html');
    echo "API SECOMP 2017 V 1.0.0";
    die();
});

//Routers API
$app->group('/api', function() use($app){
    $app->response->headers->set('Content-Type', 'application/json');

    require_once __DIR__ . '/endpoints/ticket.php';

});

// Not found
$app->notFound(function() {
    \App\Resource::response(\App\Resource::STATUS_NOT_FOUND);
});

$app->run();

