<?php

namespace App\Library;

use App\Library;
use App\Library\MyLogger as MyLogger;
use Facebook\Facebook as Facebook;
use Facebook\Exceptions\FacebookSDKException as FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException as FacebookResponseException;

class FacebookApi extends Library {


    private $fb; 
    private $app_id;
    private $app_secret;
    private $default_graph_version;
    private $log_file = 'FacebookLoggerErrors.log';

    /**
     * Constructor
     */
    public function __construct(){

        session_start();

        $ini = parse_ini_file(__DIR__ . '/../../../config/facebook.ini');

        //Inicializa Att
        $this->setAppId($ini['app_id']);
        $this->setAppSecret($ini['app_secret']);
        $this->setDefaultGraphVersion($ini['default_graph_version']);

        $fb = new Facebook([
            'app_id'     => $ini['app_id'],
            'app_secret' => $ini['app_secret'],
            'default_graph_version' => $ini['default_graph_version']
        ]);


        $this->setFb($fb);
    }

    /**
     * Get facebook service
     */
    public function init(){
    }

    /**
     * Get client metada
     */
    public function getClientMetadata($accessToken){

        $myLogger = new MyLogger();

        $response = array(
             'error' => false
            ,'data' => null
        );

        $oAuth2Client = $this->getFb()->getOAuth2Client();
        try {
          $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        } catch(FacebookResponseException $e) {
            $myLogger->register(array(
                 'HTTP Status code' => $e->getHttpStatusCode()
                ,'Error sub code'   => $e->getSubErrorCode()
                ,'Error type'       => $e->getErrorType()
                ,'Message'          => $e->getMessage()
                ,'Response data'    => $e->getResponseData()
            ), $this->log_file);
            $response['error'] = $e->getHttpStatusCode().": ".$e->getMessage();
            return $response;
        }


        $status = $this->validadeTokenFacebook($tokenMetadata);
        if(!empty($status['error'])){
            $response['error'] = $status['error'];
            return $response;
        }


        $this->validadeAppId($tokenMetadata, $this->getAppId());
        if(!empty($status['error'])){
            $response['error'] = $status['error'];
            return $response;   
        }

        $response['data'] = $tokenMetadata;
        return $response;
    }

    /**
     * Get client public profile
     */
    public function getClientPublicProfile($accessToken){

        $response = array(
             'error' => false
            ,'data' => null
        );

        try {
          $response['data'] = $this->getFb()->get('/me?fields=id,name,email,picture.width(800).height(800)', $accessToken);
        } catch(FacebookResponseException $e) {
            $myLogger->register(array(
                 'HTTP Status code' => $e->getHttpStatusCode()
                ,'Error sub code'   => $e->getSubErrorCode()
                ,'Error type'       => $e->getErrorType()
                ,'Message'          => $e->getMessage()
                ,'Response data'    => $e->getResponseData()
            ), $this->log_file);
            $response['error'] = $e->getHttpStatusCode().": ".$e->getMessage();
        } catch(FacebookSDKException $e) {
            $myLogger->register(array(
                 'Error code' => $e->getCode()
                ,'Message'    => $e->getMessage()
            ), $this->log_file);
            $response['error'] = $e->getCode().": ".$e->getMessage();
        }

        return $response;
    }

    /**
     * Validade token Facebook
     */
    public function validadeTokenFacebook($tokenMetadata){


        $myLogger = new MyLogger();

        $response = array(
            'error' => false
        );

        try{
            if($tokenMetadata->isError()){
                $myLogger->register(array(
                     'Error code'     => $tokenMetadata->getErrorCode()
                    ,'Error sub code' => $tokenMetadata->getErrorSubcode()
                    ,'Message'        => $tokenMetadata->getErrorMessage()
                ), $this->log_file);
                $response['error'] = $tokenMetadata->getErrorCode().": ".$tokenMetadata->getErrorMessage();
            }

            if($tokenMetadata->validateExpiration()){
                $response['error'] = "Unauthorized: The facebook user token is expired.";
            }
        } catch(FacebookSDKException $e){
            $myLogger->register(array(
                 'Error code'    => $e->getCode()
                ,'Message'       => $e->getMessage()
            ), $this->log_file);
            $response['error'] = $e->getCode().": ".$e->getMessage();
            return $response;
        }

        return $response;
    }

    /**
     * Validade App Id
     */
    public function validadeAppId($tokenMetadata, $appId){
        
        $myLogger = new MyLogger();

        $response = array(
            'error' => false
        );

        try {
            $tokenMetadata->validateAppId($appId);
        } catch (FacebookSDKException $e) {
            $myLogger->register(array(
                 'Error code' => $e->getCode()
                ,'Message'    => $e->getMessage()
            ), $this->log_file);
            $response['error'] = $e->getCode().": ".$e->getMessage();
        }

        return $response;
    }

    public function getFb(){
        return $this->fb;
    }

    private function setFb($fb){
        $this->fb = $fb;
    }

    public function getAppId(){
        return $this->app_id;
    }

    private function setAppId($app_id){
        $this->app_id = $app_id;
    }

    public function getAppSecret(){
        return $this->app_secret;
    }

    private function setAppSecret($app_secret){
        $this->app_secret = $app_secret;
    }

    public function getDefaultGraphVersion(){
        return $this->default_graph_version;
    }

    private function setDefaultGraphVersion($default_graph_version){
        $this->default_graph_version = $default_graph_version;
    }
}