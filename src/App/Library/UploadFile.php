<?php

namespace App\Library;

use App\Library;
use Upload\Storage\FileSystem as FileSystem;
use Upload\Validation\Mimetype as Mimetype;
use Upload\Validation\Size as Size;
use Upload\File as File;
use \Exception;

class UploadFile extends Library
{

    private $base_url;
    private $dir;

    // MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
    private $conf_files = array(
        'supplier' => array(
             'folder' => 'supplier_csv'
            ,'type' => array(
                 'csv'
                ,'txt'
            )
        )
    );

    /**
     * Construct
     */
    public function __construct(){
    	$this->base_url = 'https://'.$_SERVER['HTTP_HOST'].'/';
    	$this->dir      = __DIR__.'/../../../public/uploads/';
    }

    /**
     * Upload file
     */    
    public function uploadFile($conf_file, $file_name){

    	$directory = $this->dir.$this->conf_files[$conf_file]['folder'];
    	if(!is_dir($directory)){
        	mkdir($directory, 0777);
        }

    	$storage = new FileSystem($directory);
		$file    = new File($file_name, $storage);

		// Rename file
		$new_filename = uniqid();
		$file->setName($new_filename);

		// Validate file upload
		$file->addValidations(array(
		    // Ensure file is of type "image/png"
		    new Mimetype(array('text/csv', 'csv-schema')),

		    // Validate size (use "B", "K", M", or "G")
		    new Size('20M')
		));

		// Access data about the file that has been uploaded
		$response = array(
		    'name'       => $file->getNameWithExtension(),
		    'extension'  => $file->getExtension(),
		    'mime'       => $file->getMimetype(),
		    'size'       => $file->getSize(),
		    'md5'        => $file->getMd5(),
		    'dimensions' => $file->getDimensions()
		);

        print_r($file);
        die();
		// Try to upload file
		try {
		    $file->upload();
		} catch (\Exception $e) {
		    $response['errors'] = $file->getErrors();
		}

		return $response;
    }

    /**
     * Get file
     */    
    public function getFile($conf_file, $file_name){

        return $this->base_url.'uploads/'.$this->conf_files[$conf_file]['folder'].'/'.$file_name;
    }

    /**
     * Delete file
     */
    public function deleteFile($conf_file, $file_name){
        
        $path = $this->dir.$this->conf_files[$conf_file]['folder'].'/'.$file_name;
        @unlink($path);
    }
}