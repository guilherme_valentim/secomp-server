<?php

namespace App\Library;

use App\Library;
use PHPMailer;


class Email extends Library
{

    private $emailSender = 'guilherme@pluritech.com.br';
    private $nomeSender  = 'Drinkeat';
    private $base_url;
    private $mailer;
    
    /**
     * Construct
     */
    public function __construct(){

        $this->base_url = 'http://'.$_SERVER['HTTP_HOST'].'/';
        $this->mailer = new PHPMailer();
        $this->mailer->isSMTP();
        $this->mailer->Host = "www.pluritech.com.br";
        $this->mailer->SMTPAuth = true;
        $this->mailer->Username = 'guilherme@pluritech.com.br';
        $this->mailer->Password = 'guigo86589511';
        $this->mailer->Port     = 587;
        $this->mailer->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $this->mailer->From = "guilherme@pluritech.com.br"; 
        $this->mailer->FromName = "Drinkeat"; 
    }

    public function sendEmail($email, $mensagem, $assunto, $assinatura = false){
        
        $mail = $this->getMailer();

        // Define os destinatários
        if(is_array($email)){
            foreach($email as $key => $valor){
                $mail->AddAddress($valor);
            }
        } else{            
            $mail->AddAddress($email);
        }

        // Dados tecnicos da mensagem
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';

        if($assinatura){
            $mensagem .= "<p>Atenciosamente</p>";
            $mensagem .= "<p><img src='http://www.pluritech.com.br/img/uploads/assinatura_netmaquinas.png'</img>";
        } 

        // Mensagem
        $mail->Subject  = $assunto;
        $mail->Body     = $mensagem;

        // Envio
        $enviado = $mail->Send();

        //Limpa destinatários e anexos
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();

        // Exibe uma mensagem de resultado
        if ($enviado) {
            return array(
                'status' => true,
                'alert'  => ""
            );
        } else {
            return array(
                'status' => false,
                'alert'  => "Não foi possível enviar o e-mail. Informações do erro: ".$mail->ErrorInfo
            );
        }
    }

    public function getBaseUrl(){

        return $this->base_url;
    }

    public function getMailer(){

        return $this->mailer;
    }

    public function getEmailSender(){

        return $this->emailSender;
    }
}