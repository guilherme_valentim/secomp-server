<?php

namespace App\Library;

use App\Library;
use ElephantIO\Client as ElephantClient;
use ElephantIO\Engine\SocketIO\Version1X as ElephantVersion;
use ElephantIO\Exception\SocketException as SocketException;
use \Exception;

class Socket extends Library
{

    private $host; //Trocar para HTTPS quando for para produção. (Construct host)
    private $port;
    private $log_error_patch = '../logs/SOCKET_errors.log';

    /**
     * Construct
     */
    public function __construct(){
		$this->host   = 'http://'.$_SERVER['HTTP_HOST'].'';
		$this->port   = '8800'; 
    }

    /**
     * Emite uma mensagem para um canal e fecha a conexão.
     */    
    public function sendMessageToChannel($channel, $data){

    	$return = array(
    		'error'  => false
    	);

		try{
	    	$client = new ElephantClient(new ElephantVersion($this->host.':'.$this->port));
	        $client->initialize();
	        $client->emit($channel, $data);
	        $client->close();
        } catch(Exception $e){
        	$this->error_socket($e->getCode(), $e->getMessage(), $data);
			$return['error'] = array(
				"description" => $e->getMessage()
			);
    	}

    	return $return;
    }

    /**
     * Error Socket
     * @param $message
     * @param $data
     * @return void
     */
    public function error_socket($code, $message, $data){

        $filename = $this->log_error_patch;
        $fp       = fopen($filename, "a");
        $txt_data = is_array($data)  ? print_r($data,  true) : $data;

        fwrite($fp, "\n 
        BEGIN======*
            DATE-TIME: ".date('d/m/Y - H:i:s')."
            MESSAGE: ".$message."
            CODE: ".$code."
            DATA: ".$txt_data."
        END========*
        ");

        fclose($fp);
    }
}