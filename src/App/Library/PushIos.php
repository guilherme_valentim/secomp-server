<?php

namespace App\Library;

use App\Library;
use App\Library\MyApnsLogger as MyApnsLogger;
use \Exception;

class PushIos extends Push
{
    /*
        Há duas constantes nesse Abstract:
        ENVIRONMENT_PRODUCTION
        ENVIRONMENT_SANDBOX
        Para trocar pro ENVIRONMENT_PRODUCTION, deverá, antes, ser criado uma ".pem" de produção
        Dois arquivos de configuração
    */
    private $environment;
    private $pem_patch     = '../pem_files/piloto31.pem';
    private $pem_authority = '../pem_files/entrust_root_certification_authority.pem';


    /**
     * Construct
     */
    public function __construct(){
        if($_SERVER['HTTP_HOST'] == 'piloto31-server.pluritech.com.br'){
            $this->environment = \ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION;
        } else{
            $this->environment = \ApnsPHP_Abstract::ENVIRONMENT_SANDBOX;
        }
    }

    /**
     * Envia notificaçao de nova corrida
     */
    public function sendNewTravelNotification($travel, $device){
        $message = array(
             'id'       => self::NEW_TRAVEL
            ,'title'    => "Cliente chamando!"
            ,'message'  => $travel['start_long_description']
            ,'stateParams' => array(
                 'idTravel'            => $travel['id_travel']
                ,'idClient'            => $travel['client']['id_client']
                ,'idNotificationType'  => self::NEW_TRAVEL
                ,'dtMessage'           => time()
            )
        );
        $this->send($device, $message);
    }

    /**
     * Envia notificaçao de corrida aceita
     */
    public function sendAcceptedTravelNotification($travel, $device){

        $message = array(
             'id'       => self::ACCEPTED_TRAVEL
            ,'title'    => "Viagem aceita."
            ,'message'  => "Sua viagem foi aceita pelo ".$travel['biker']['name']
            ,'stateParams' => array(
                 'idTravel'            => $travel['id_travel']
                ,'idClient'            => $travel['client']['id_client']
                ,'idBiker'             => $travel['biker']['id_biker']
                ,'idNotificationType'  => self::ACCEPTED_TRAVEL
                ,'dtMessage'           => time()
            )
        );

        $this->send($device, $message);
    }

    /**
     * Send driver arrived notification
     */
    public function sendDriverArrivedNotification($travel, $device){

        $message = array(
             'id'       => self::DRIVER_ARRIVED
            ,'title'    => "Seu motociclista chegou."
            ,'message'  => "O motociclista ".$travel['biker']['name']." esta te aguardando."
            ,'stateParams' => array(
                 'idTravel'            => $travel['id_travel']
                ,'idClient'            => $travel['client']['id_client']
                ,'idBiker'             => $travel['biker']['id_biker']
                ,'idNotificationType'  => self::DRIVER_ARRIVED
                ,'dtMessage'           => time()
            )
        );
        $this->send($device, $message);
    }

    /**
     * Envia notificaçao de corrida finalizada.
     */
    public function sendFinishedTravelNotification($travel, $device){
        $message = array(
             'id'       => self::FINISHED_TRAVEL
            ,'title'    => "Avalie o motociclista."
            ,'message'  => "Por favor avalie como foi a sua viagem."
            ,'stateParams' => array(
                 'idTravel'            => $travel['id_travel']
                ,'idClient'            => $travel['client']['id_client']
                ,'idBiker'             => $travel['biker']['id_biker']
                ,'idNotificationType'  => self::FINISHED_TRAVEL
                ,'dtMessage'           => time()
            )
        );
        $this->send($device, $message);
    }

    /**
     * Envia notificaçao de corrida iniciada.
     */
    public function sendStartedTravelNotification($travel, $device){

        $message = array(
             'id'       => self::FINISHED_TRAVEL
            ,'title'    => "Viagem iniciada."
            ,'message'  => "O motorista iniciou a sua viagem."
            ,'stateParams' => array(
                 'idTravel'            => $travel['id_travel']
                ,'idClient'            => $travel['client']['id_client']
                ,'idBiker'             => $travel['biker']['id_biker']
                ,'idNotificationType'  => self::IN_ROUTE
                ,'dtMessage'           => time()
            )
        );
        $this->send($device, $message);
    }

    /**
     * Envia notificaçao de corrida cancelada
     */
    public function sendCanceledTravelNotification($travel, $device){
        $message = array(
             'id'       => self::CANCELED_TRAVEL
            ,'title'    => "Viagem cancelada."
            ,'message'  => "O cliente cancelou a viagem."
            ,'stateParams' => array(
                 'idTravel'            => $travel['id_travel']
                ,'idClient'            => $travel['client']['id_client']
                ,'idBiker'             => $travel['biker']['id_biker']
                ,'idNotificationType'  => self::CANCELED_TRAVEL
                ,'dtMessage'           => time()
            )
        );
        $this->send($device, $message);
    }

    /**
     * Envia notificaçao de mensagem do piloto ao cliente
     */
    public function sendBikerMessage($travel, $device, $message){

        $message = array(
             'id'       => self::DRIVER_MSG
            ,'title'    => $travel['biker']['name']
            ,'message'  => $message
            ,'stateParams' => array(
                 'idTravel'            => $travel['id_travel']
                ,'idClient'            => $travel['client']['id_client']
                ,'idBiker'             => $travel['biker']['id_biker']
                ,'bikerMessage'        => $message
                ,'idNotificationType'  => self::DRIVER_MSG
                ,'dtMessage'           => time()
            )
        );
        $this->send($device, $message);
    }

    /**
     * Envia notificaçao de mensagem do cliente ao piloto
     */
    public function sendClientMessage($travel, $device, $message){

        $message = array(
             'id'       => self::CLIENT_MSG
            ,'title'    => $travel['client']['name']
            ,'message'  => $message
            ,'stateParams' => array(
            	 'idTravel'            => $travel['id_travel']
                ,'idClient'            => $travel['client']['id_client']
                ,'idBiker'             => $travel['biker']['id_biker']
            	,'bikerMessage'        => $message
                ,'idNotificationType'  => self::CLIENT_MSG
            	,'dtMessage'           => time()
            )
        );
        $this->send($device, $message);
    }

    /**
     * Envia push para um device
     * @param $mensagem = array('title', 'message', 'id')
     */
    public function send($device_registration_id, $mensagem){

        $MyApnsLogger = new MyApnsLogger();
        $push = new \ApnsPHP_Push($this->environment, $this->pem_patch);
        $push->setLogger($MyApnsLogger);

        $push->setRootCertificationAuthority($this->pem_authority);
        
        $push->connect();

        /* Seta REGID do celular a ser enviado. */
        $message = new \ApnsPHP_Message($device_registration_id);

        $message->setCustomIdentifier($mensagem['id']); //ID LIKE COLAPS
        
        /* Seta a nova mensagem */
        $message->setText($mensagem['title']."\n".$mensagem['message']);
        
        /* Seta o som padrão de notificação do dispositivo */
        $message->setSound();

        /* Seta dados adicionais pra enviar no JSON que chega ao dispositivo */
        if(isset($mensagem['stateParams'])){
            foreach($mensagem['stateParams'] as $key => $valor){
                $message->setCustomProperty($key, $valor);
            }
        }
        
        // Seta o tempo de expiração em 30 segudos
        $message->setExpiry(30);
        
        $push->add($message);
        try{
            $push->send();
        }  catch (\Exception $e) {
            echo "exception comum";
            var_dump($e);
            error_log($e);
        }
        $push->disconnect();
    }
}