<?php

namespace App\Library;

use App\Library;
use App\Service\Authorize as AuthorizeService;
use App\Service\User as UserService;

class Authorize extends Library
{
    /**
     * @var \App\Service\Auth
     */
    private $authorizeService;

    /**
     * Get auth service
     */
    public function init(){

       $this->setAuthorizeService(new AuthorizeService());
    }

    /**
     * @return bool
     */
    public function verifyTokenUser($token = null, $stop = true){

        if(!empty($token)){
            $user = $this->getAuthorizeService()->verifyTokenUser($token);
            if($user){
                $env         = $this->getSlim()->environment();
                $env['user'] = $user;
            } else {
                self::response(self::STATUS_BAD_REQUEST, array(
                    'status' =>  "Unauthorized",
                    'message' => "Invalid token."
                    )
                );
                $this->getSlim()->stop();
            }
        } else {
            self::response(self::STATUS_BAD_REQUEST, array(
                'status' =>  "BadRequest",
                'message' => "Missing token"
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * @return \App\Service\User
     */
    public function getAuthorizeService(){

        return $this->authorizeService;
    }

    /**
     * @param \App\Service\User $userService
     */
    public function setAuthorizeService($authorizeService){

        $this->authorizeService = $authorizeService;
    }
}