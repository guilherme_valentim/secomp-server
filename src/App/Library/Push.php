<?php

namespace App\Library;

use App\Library;
use App\Library\PushAndroid as PushAndroid;
use App\Library\PushIos     as PushIos;

class Push extends Library
{

    const DRIVER_ARRIVED   = 'DRIVER_ARRIVED';
    const NEW_TRAVEL       = 'NEW_TRAVEL';
    const ACCEPTED_TRAVEL  = 'ACCEPTED_TRAVEL';
    const CANCELED_TRAVEL  = 'CANCELED_TRAVEL';
    const IN_ROUTE         = 'IN_ROUTE';
    const FINISHED_TRAVEL  = 'FINISHED_TRAVEL';
    const DRIVER_MSG       = 'DRIVER_MSG';
    const CLIENT_MSG       = 'CLIENT_MSG';
    
    /**
     * Construct
     */
    public function __construct(){

    }

    /**
     * Identifica plataforma
     */
    public function identifyPlatform($platform){

        if($platform == 'A'){
            return $pushLibrary = new PushAndroid();
        } else if($platform == 'I'){
            return $pushLibrary = new PushIos();
        } else{
            return false;
        }
    }
}