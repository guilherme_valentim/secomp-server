<?php

namespace App\Library;

use App\Library;
use \Exception;

class GeneratorImage extends Library
{

    private $base_url;
    private $water_mark;
    
    private $conf_pictures = array(
        'customer' => array(
            'folder'       => 'customer',
            'width'        => 200,
            'height'       => 200,
            'thumb'        => false,
            'water_mark'   => false,
            'transparency' => false
        ),
        'event' => array(
            'folder'       => 'event',
            'width'        => 650,
            'height'       => 310,
            'thumb'        => true,
            'thumb_width'  => 25,
            'thumb_height' => 12,
            'water_mark'   => false,
            'transparency' => false
        ),
        'bank' => array(
            'folder'       => 'bank',
            'width'        => 150,
            'height'       => 150,
            'thumb'        => false,
            'water_mark'   => false,
            'transparency' => false
        )
    );

    private $accept_type = array(
        'image/png'  => 'png',
        'image/jpeg' => 'jpg' 
    );

    /**
     * Construct
     */
    public function __construct(){
        $this->base_url   = 'https://'.$_SERVER['HTTP_HOST'].'/';
        $this->water_mark = __DIR__.'/../../../public/uploads/institutional/marca_agua.png';
    }

    /**
     * Cria uma imagem.
     */    
    public function generateImage($conf_picture, $base64){

        $error      = array();
        $image_name = null;
        $dir        = __DIR__.'/../../../public/uploads/'.$this->conf_pictures[$conf_picture]['folder'].'/';

        try{
            $image_data = base64_decode($base64);
            $f          = finfo_open();
            $mime_type  = finfo_buffer($f, $image_data, FILEINFO_MIME_TYPE);

            if(!array_key_exists($mime_type, $this->accept_type)){
                $error[] = array(
                    "code"        => "F01",
                    "description" => "Format not accepted. Try again using gif, png or jpg"
                );
            } else{
                $photo = imagecreatefromstring($image_data);
                $photo = $this->resize($photo, $this->conf_pictures[$conf_picture]);

                if(!empty($photo['error'])){
                    $error = array_merge($error, $photo['error']);
                } else{
                    if(!file_exists($dir)){
                        mkdir($dir, 0777);
                    }

                    //Verifica se usa marca d'agua
                    if(!empty($this->conf_pictures[$conf_picture]['water_mark'])){
                        $photo['picture'] = $this->putWaterMark($mime_type, $photo['picture']);
                    }

                    $image_name = $this->generateUniqueName($mime_type);

                    if($this->accept_type[$mime_type] == 'jpg'){
                        imagejpeg($photo['picture'],$dir.$image_name, 100);
                    } else{
                        imagepng($photo['picture'],$dir.$image_name);
                    }

                    if($this->conf_pictures[$conf_picture]['thumb']){
                        $this->createThumb($conf_picture, $dir, $image_name, $mime_type);
                    }
                }
            }
        } catch(Exception $e){
            $error[] = array(
                "code"        => "F00",
                "description" => "Erro desconhecido.".$e->getMessage()
            );
        }

        return array(
            'picture_name' => $image_name,
            'error'        => $error
        );
    }

    public function resize($photo, $conf_picture){

        $error = array();
        $width_origem    = imagesx($photo);
        $height_origem   = imagesy($photo);

        $width_destino   = $conf_picture['width'];
        $height_destino  = $conf_picture['height'];

        $prop_origem     = $width_origem / $height_origem;
        $prop_destino    = $width_destino / $height_destino;

        $picture = null;

        //Validação básica
        if ($height_destino > $height_origem && $width_destino > $width_origem) {
            $error[] = array(
                "code"        => "F02",
                "description" => "Resolução mínima não atingida. Resolução indicada: ".$width_destino." x ".$height_destino."."
            );
        }
        else if($height_destino == $height_origem && $width_destino == $width_origem){
            $picture = $photo;
        } else{
            if ($height_origem >= $height_destino && $width_origem < $width_destino) { // redimensionar imagem origem para altura destino (mantendo proporção de origem)
                $picture = $this->resize_by_height(
                     $conf_picture
                    ,$photo
                    ,array('height' => $height_origem, 'width' => $width_origem)
                    ,array('height' => $height_destino, 'width' => $width_destino)
                );
            }
            else if ($height_origem < $height_destino && $width_origem >= $width_destino) { // redimensionar imagem origem para largura destino (mantendo proporção de origem)
                $picture = $this->resize_by_width(
                     $conf_picture
                    ,$photo
                    ,array('height' => $height_origem, 'width' => $width_origem)
                    ,array('height' => $height_destino, 'width' => $width_destino)
                );
            }
            else {
                if ($prop_origem > $prop_destino) { // redimensionar imagem origem para altura destino (mantendo proporção de origem) 
                    $picture = $this->resize_by_height(
                         $conf_picture
                        ,$photo
                        ,array('height' => $height_origem, 'width' => $width_origem)
                        ,array('height' => $height_destino, 'width' => $width_destino)
                    );
                } else { // redimensionar imagem origem para largura destino (mantendo proporção de origem)
                    $picture = $this->resize_by_width(
                         $conf_picture
                        ,$photo
                        ,array('height' => $height_origem, 'width' => $width_origem)
                        ,array('height' => $height_destino, 'width' => $width_destino)
                    );
                }
            }
        }

        return array(
            'picture' => $picture,
            'error'   => $error
        );
    }

    public function resize_by_height($conf_picture, $photo, $origem, $destino){
        
        $altura_calculada  = $destino['height']; // iguala altura da origem pro destino
        $largura_calculada = intval(($origem['width'] * $altura_calculada) / $origem['height']);

        if ($destino['height'] === $origem['height']) {
            $new_proportional_photo = $photo;
        } else {
            $new_proportional_photo = imagecreatetruecolor($largura_calculada, $altura_calculada); // imagem diminuida de forma proporcional

            if($conf_picture['transparency']){
                $this->setTransparency($new_proportional_photo);
            } else{
                $this->setDefaultBackgroundColor($new_proportional_photo);
            }

            imagecopyresampled($new_proportional_photo, $photo, 0, 0, 0, 0, $largura_calculada, $altura_calculada, $origem['width'], $origem['height']);
        }
        
        $dif = $largura_calculada - $destino['width'];
        if ($dif > 0) {
            //Cortada a imagem
            $foto_final = imagecreatetruecolor($destino['width'], $destino['height']);

            if($conf_picture['transparency']){
                $this->setTransparency($foto_final);
            } else{
                $this->setDefaultBackgroundColor($foto_final);
            }
            imagecopyresampled($foto_final, $new_proportional_photo, 0, 0, ($dif/2), 0, $destino['width'], $destino['height'], $destino['width'], $destino['height']);
        } else {
            $x = round($dif / 2) * -1; // coeficiente de centralização
            $foto_final = imagecreatetruecolor($destino['width'], $destino['height']);

            if($conf_picture['transparency']){
                $this->setTransparency($foto_final);
            } else{
                $this->setDefaultBackgroundColor($foto_final);
            }
            imagecopyresampled($foto_final, $new_proportional_photo, $x, 0, 0, 0, $largura_calculada, $altura_calculada, $largura_calculada, $altura_calculada); 
        }

        return $foto_final;
    }

    public function resize_by_width($conf_picture, $photo, $origem, $destino){
        
        $largura_calculada = $destino['width']; // iguala altura da origem pro destino
        $altura_calculada  = intval(($origem['height'] * $largura_calculada) / $origem['width']);

        if ($destino['width'] === $origem['width']) { 
            $new_proportional_photo = $photo;
        } else {
            $new_proportional_photo = imagecreatetruecolor($largura_calculada, $altura_calculada);

            if($conf_picture['transparency']){
                $this->setTransparency($new_proportional_photo);
            } else{
                $this->setDefaultBackgroundColor($new_proportional_photo);
            }

            imagecopyresampled($new_proportional_photo, $photo, 0, 0, 0, 0, $largura_calculada, $altura_calculada, $origem['width'], $origem['height']);
        }
        
        $dif = $altura_calculada - $destino['height'];
        if ($dif > 0) { 
            //Corta a imagem
            $foto_final = imagecreatetruecolor($destino['width'], $destino['height']);

            if($conf_picture['transparency']){
                $this->setTransparency($foto_final);
            } else{
                $this->setDefaultBackgroundColor($foto_final);
            }

            imagecopyresampled($foto_final, $new_proportional_photo, 0, 0, 0, ($dif/2), $destino['width'], $destino['height'], $destino['width'], $destino['height']);
        } else {
            $y = round($dif / 2) * -1; // coeficiente de centralização
            $foto_final = imagecreatetruecolor($destino['width'], $destino['height']);

            if($conf_picture['transparency']){
                $this->setTransparency($foto_final);
            } else{
                $this->setDefaultBackgroundColor($foto_final);
            }

            imagecopyresampled($foto_final, $new_proportional_photo, 0, $y, 0, 0, $largura_calculada, $altura_calculada, $largura_calculada, $altura_calculada); 
        }

        return $foto_final;
    }

    /**
     * Recupera uma imagem
     */    
    public function getImage($conf_picture, $image){

        return $this->base_url.'uploads/'.$this->conf_pictures[$conf_picture]['folder'].'/'.$image;
    }

    /*
     * Insere a marca de àgua na imagem
     */
    public function putWaterMark($mime_type, $photo){

        $padding   = 10;
        $opacidade = 50;

        $mark = imagecreatefrompng($this->water_mark);
        imagealphablending($mark, FALSE);
        imagesavealpha($mark, TRUE);

        $imagem = $photo;

        $mark_size   = getimagesize($this->water_mark);
        $mark_width  = $mark_size[0];
        $mark_height = $mark_size[1];

        $dest_x  = imagesx($photo) - $mark_width - $padding;
        $dest_y  = $mark_height - $padding;

        imagecopy($imagem,$mark,$dest_x,$dest_y,0,0,$mark_width,$mark_height);
        
        return $imagem;
    }

    /*
     * Recupera a thumb de uma imagem
     */
    public function getThumb($image){

        $image_thumb  = str_replace(".", "_thumb.", $image);

        return $image_thumb;
    }

    /**
     * Delete image
     */
    public function deleteImage($conf_picture, $image){

        if($this->conf_pictures[$conf_picture]['thumb']){
            $image_thumb  = str_replace(".", "_thumb.", $image);
            $path_thumb   = __DIR__.'/../../../public/uploads/'.$this->conf_pictures[$conf_picture]['folder'].'/'.$image_thumb;
            @unlink($path_thumb);
        }
        
        $path_picture = __DIR__.'/../../../public/uploads/'.$this->conf_pictures[$conf_picture]['folder'].'/'.$image;
        @unlink($path_picture);
    }

    /**
     * Gera um nome unico para a imagem.
     */
    public function generateUniqueName($mime_type){
        $temp_name = rand(1, 50).microtime();
        $name = date('Ymd')."_".md5($temp_name).".".$this->accept_type[$mime_type];
        return $name;
    }

    /**
     * Gera uma thumb para a imagem.
     */
    public function createThumb($conf_picture, $dir, $image_name, $mime_type){
        $image_thumb_name     = str_replace(".".$this->accept_type[$mime_type], "_thumb.".$this->accept_type[$mime_type], $image_name);
        list($width, $height) = getimagesize($dir.$image_name);

        $image_p = imagecreatetruecolor($this->conf_pictures[$conf_picture]['thumb_width'], $this->conf_pictures[$conf_picture]['thumb_height']);
        $this->setTransparency($image_p);

        if($this->accept_type[$mime_type] == 'jpg'){
            $image = imagecreatefromjpeg($dir.$image_name);
        } else if($this->accept_type[$mime_type] == 'png'){
            $image = imagecreatefrompng($dir.$image_name);
        } else{
            $image = imagecreatefromgif($dir.$image_name);
        }
        
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $this->conf_pictures[$conf_picture]['thumb_width'], $this->conf_pictures[$conf_picture]['thumb_height'], $width, $height);

        if($this->accept_type[$mime_type] == 'jpg'){
            imagejpeg($image_p, $dir.$image_thumb_name, 100);
        } else if($this->accept_type[$mime_type] == 'png'){
            imagepng($image_p, $dir.$image_thumb_name);
        } else{
            imagegif($image_p, $dir.$image_thumb_name, 100);
        }
    }

    /**
     * Set transparency
     */
    public function setTransparency(&$photo){
        $color = imagecolorallocatealpha($photo, 0, 0, 0, 127);
        imagefill($photo, 0, 0, $color);
        imagesavealpha($photo, true);
    }

    /**
     * Set default background color
     * http://www.uff.br/cdme/matrix/matrix-html/matrix_color_cube/matrix_color_cube_br.html
     */
    public function setDefaultBackgroundColor(&$photo){
        $color = imagecolorallocate($photo, 0, 0, 0);
        imagefill($photo, 0, 0, $color);
    }
}