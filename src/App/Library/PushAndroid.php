<?php

namespace App\Library;

use App\Library;
use \PHP_GCM\Sender as Sender;
use \PHP_GCM\Message as Message;
use \Exception;
use \InvalidArgumentException;

class PushAndroid extends Push
{
    
    private $gcm_api_key;
    private $multicast_result;

    /**
     * Construct
     */ 
    public function __construct(){
        $this->setGcmApiKey('AIzaSyB60xWUWaIbRrBggIkTofwCrzuFC8P6yRE');
        $this->setMulticastResult(5);
    }

    /**
     * Envia notificaçao de nova mensagem
     */
    public function sendNewTravelNotification($travel, $device){

        $message = array(
             'id'       => self::NEW_TRAVEL
            ,'title'    => "Cliente chamando!"
            ,'message'  => $travel['start_long_description']
            ,'idTravel' => $travel['id_travel']
            ,'idClient' => $travel['client']['id_client']
            ,'idNotificationType' => self::NEW_TRAVEL
            ,'image'     => $travel['client']['picture_url']
            ,'dtMessage' => time()
        );

        $this->send($message, $device);
    }

    /**
     * Envia notificaçao de viagem aceita
     */
    public function sendAcceptedTravelNotification($travel, $device){

            $message = array(
                 'title'    => "Viagem aceita."
                ,'message'  => "Sua viagem foi aceita pelo ".$travel['biker']['name']
                ,'idTravel' => $travel['id_travel']
                ,'idClient' => $travel['client']['id_client']
                ,'idBiker'  => $travel['biker']['id_biker']
                ,'image'    => $travel['biker']['picture_url']
                ,'dtMessage' => time()
                ,'idNotificationType' => self::ACCEPTED_TRAVEL
            );

            $this->send($message, $device);
    }

    /**
     * Send driver arrived notification
     */
    public function sendDriverArrivedNotification($travel, $device){

            $message = array(
                 'title'     => "Seu motociclista chegou."
                ,'message'   => "O motorista ".$travel['biker']['name']." está te aguardando."
                ,'idTravel'  => $travel['id_travel']
                ,'idClient'  => $travel['client']['id_client']
                ,'idBiker'   => $travel['biker']['id_biker']
                ,'image'     => $travel['biker']['picture_url']
                ,'dtMessage' => time()
                ,'idNotificationType' => self::DRIVER_ARRIVED
            );

            $this->send($message, $device);
    }

    /**
     * Envia notificaçao de viagem finalizada.
     */
    public function sendFinishedTravelNotification($travel, $device){

            $message = array(
                 'title'   => "Avalie o motociclista."
                ,'message' => "Por favor avalie como foi a sua viagem."
                ,'idTravel' => $travel['id_travel']
                ,'idClient' => $travel['client']['id_client']
                ,'idBiker' => $travel['biker']['id_biker']
                ,'image'     => $travel['biker']['picture_url']
                ,'dtMessage' => time()
                ,'idNotificationType' => self::FINISHED_TRAVEL
            );

            $this->send($message, $device);
    }

    /**
     * Envia notificaçao de viagem iniciada.
     */
    public function sendStartedTravelNotification($travel, $device){

            $message = array(
                 'title'     => "Viagem iniciada."
                ,'message'   => "O motorista iniciou a sua viagem."
                ,'idTravel'  => $travel['id_travel']
                ,'idClient'  => $travel['client']['id_client']
                ,'idBiker'   => $travel['biker']['id_biker']
                ,'image'     => $travel['biker']['picture_url']
                ,'dtMessage' => time()
                ,'idNotificationType' => self::IN_ROUTE
            );

            $this->send($message, $device);
    }

    /**
     * Envia notificaçao de viagem cancelada
     */
    public function sendCanceledTravelNotification($travel, $device){

            $message = array(
                 'title'   => "Viagem cancelada."
                ,'message'  => "O cliente cancelou a viagem."
                ,'idTravel' => $travel['id_travel']
                ,'idClient' => $travel['client']['id_client']
                ,'idBiker' => $travel['biker']['id_biker']
                ,'dtMessage' => time()
                ,'image'     => $travel['client']['picture_url']
                ,'idNotificationType' => self::CANCELED_TRAVEL
            );

            $this->send($message, $device);
    }

    /**
     * Envia notificaçao de mensagem do piloto
     */
    public function sendBikerMessage($travel, $device, $message){

        $message = array(
             'title'        => $travel['biker']['name']
            ,'message'      => $message
            ,'idTravel'     => $travel['id_travel']
            ,'idClient'     => $travel['client']['id_client']
            ,'idBiker'      => $travel['biker']['id_biker']
            ,'bikerMessage' => $message
            ,'dtMessage'    => time()
            ,'image'        => $travel['biker']['picture_url']
            ,'idNotificationType' => self::DRIVER_MSG
        );

        $this->send($message, $device);
    }

    /**
     * Envia notificaçao de mensagem do cliente ao piloto
     */
    public function sendClientMessage($travel, $device, $message){

        $message = array(
             'title'        => $travel['client']['name']
            ,'message'      => $message
            ,'idTravel'     => $travel['id_travel']
            ,'idClient'     => $travel['client']['id_client']
            ,'idBiker'      => $travel['biker']['id_biker']
            ,'bikerMessage' => $message
            ,'dtMessage'    => time()
            ,'image'        => $travel['client']['picture_url']
            ,'idNotificationType' => self::CLIENT_MSG
        );

        $this->send($message, $device);
    }


    /**
     * Envia push para um device
     */
    public function send($pay_load_data, $device_registration_id){
        
        
        $pay_load_data['priority'] = 'high';
        $sender  = new Sender($this->getGcmApiKey());
        $message = new Message('1', $pay_load_data);
        $code = 200;

        try {
            $result = $sender->send($message, $device_registration_id, $this->GetMulticastResult());
        } catch (\InvalidArgumentException $e) {
            $code = $e->getCode();
            error_log($code);
        } catch (PHP_GCM\InvalidRequestException $e) {
            $code = $e->getCode();
            error_log($code);
        } catch (\Exception $e) {
            $code = $e->getCode();
            error_log($code);
        }

        return $code;
    }

    /**
     * Envia push para n device's
     */
    public function sendMulti($pay_load_data, $devices){
        
        $sender  = new Sender($this->getGcmApiKey());
        $message = new Message('1', $pay_load_data);
        $alert   = array();

        try {
            $result = $sender->sendMulti($message, $devices, $this->GetMulticastResult());
            //$result = $sender->send($message, array(), 2);
        } catch (\InvalidArgumentException $e) {
            echo "caiu no InvalidArgumentException";
            //$alert[] = var_dump($e);
            //die();
        } catch (PHP_GCM\InvalidRequestException $e) {
            // server returned HTTP code other than 200 or 503
            echo "caiu no InvalidRequestException do PHP_GCM";
            //var_dump($e);  
            //die();
        } catch (\Exception $e) {
            //echo $e->getCode();
            //echo $e->getMessage();
            //echo $e->getFile();
            //echo $e->getLine();
            //echo "exception comum";
            //echo $e->getMessage();   
            //die();
        }

        die();
    }

    public function setGcmApiKey($gcm_api_key){

        $this->gcm_api_key = $gcm_api_key;
    }

    public function getGcmApiKey(){
        return $this->gcm_api_key;
    }    

    public function setMulticastResult($multicast_result){
        $this->multicast_result = $multicast_result;
    }

    public function GetMulticastResult(){
        return $this->multicast_result;
    }
}