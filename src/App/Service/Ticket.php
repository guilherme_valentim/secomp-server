<?php

namespace App\Service;

use App\Service;
use PDO;

class Ticket extends Service
{

    public $log_file = "ticket.log";


    /**
     * Register Ticket
     * @return array
     */
    public function registerTicket($params){
        
        $conn = $this->getConnection();

        $query = $conn->prepare("
			INSERT INTO ticket
				(subject, owner, description, observation)
			VALUES
				(:subject, :owner, :description, :observation)
        ");

        $query->bindParam(':subject', $params['subject']);
        $query->bindParam(':owner', $params['owner']);
        $query->bindParam(':description', $params['description']);
        $query->bindValue(':observation', !empty($params['observation']) ? $params['observation'] : null);

        $query->execute();
        $status = $this->verifyBadExecute($query);
        $id     = $conn->lastInsertId('id_ticket_seq');
        $conn   = null;

        if($status){
            return $id;
        } else{
            return $status;
        }
    }

    /*
     * Update ticket
     */
    public function updateTicket($id_ticket, $params){

        
    }

    /*
     * Delete ticket
     */
    public function deleteTicket($id_ticket){

        
    }

    /**
     * Get ticket
     * @return array
     */
    public function getTicket($id_ticket){


    }

    /**
     * Get Count tickets
     * @return array
     */
    public function getCountTickets($params){
        
    }

    /**
     * Get all tickets
     * @return array
     */
    public function getTickets($params){
        
    }
}