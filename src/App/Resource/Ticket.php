<?php

namespace App\Resource;

use App\Resource;
use App\Service\Ticket   as TicketService;
use App\Library\MyLogger as MyLogger;


class Ticket extends Resource
{
    /**
     * @var \App\Service\Ticket
     */
    private $ticketService;
    private $log_file = "ticket_resource.log";

    /**
     * Get ticket service
     */
    public function init(){

        $this->setTicketService(new TicketService());
    }

    /**
     * Register ticket
     */
    public function post(){

    }

    /**
     * Register ticket
     */
    public function checkParamsCreate($obj_params){

    }

    /**
     * Update ticket
     */
    public function put($id_ticket){

    }

    /**
     * Update ticket
     */
    public function checkParamsUpdate($obj_params){


    }

    /**
     * Update ticket
     */
    public function patch($id_ticket){

        $obj_params = $this->getBodyRequest();

        $ticket = $this->getTicketService()->getTicket($id_ticket);
        if(!$ticket){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "BadRequest",
                'message'  => "Invalid ticket.",
                )
            );
            $this->getSlim()->stop();
        } else {
            $obj_params = $this->getParamsToUpdate($id_ticket, $ticket, $obj_params);
        }

        $status = $this->getTicketService()->updateTicket($id_ticket, $obj_params);
        if(!$status){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status'   => "InternalServerError",
                'message'  => "Internal server error on update ticket."
                )
            );
            $this->getSlim()->stop();
        }

        self::response(self::STATUS_NO_CONTENT, array());
    }

    /**
     * Get params to update
     */
    public function getParamsToUpdate($id_ticket, $ticket, $params){

        $params['subject']     = !empty($params['subject'])     ? $params['subject']     : $ticket['subject'];
        $params['owner']       = !empty($params['owner'])       ? $params['owner']       : $ticket['owner'];
        $params['description'] = !empty($params['description']) ? $params['description'] : $ticket['description'];
        $params['status']      = !empty($params['status'])      ? $params['status']      : $ticket['status'];
        $params['observation'] = !empty($params['observation']) ? $params['observation'] : $ticket['observation'];

        return $params;
    }

    /**
     * Delete ticket
     */
    public function delete($id_ticket){

    }

    /**
     * Get ticket
     */
    public function getTicket($id_ticket){

    }

    /**
     * Get tickets
     */
    public function getTickets(){

    }

    /**
     * Show options in header
     */
    public function options(){

        self::response(self::STATUS_OK, array(), array('POST', 'OPTIONS'));
    }

    /**
     * @return \App\Service\Ticket
     */
    public function getTicketService(){

        return $this->ticketService;
    }

    /**
     * @param \App\Service\AUthorize $ticketService
     */
    public function setTicketService($ticketService){

        $this->ticketService = $ticketService;
    }

    /**
     * @return array
     */
    public function getOptions(){

        return $this->options;
    }
}